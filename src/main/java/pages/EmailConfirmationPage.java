package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EmailConfirmationPage {
    private WebDriver driver;
    private By content = By.id("content");

    public EmailConfirmationPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getConfirmationText(){
        return driver.findElement(content).getText();
    }
}
