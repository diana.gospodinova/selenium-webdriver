package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MenuElementsPage {
    private WebDriver driver;

    public MenuElementsPage(WebDriver driver) {
        this.driver = driver;
    }

    public int countElementsByTagName(String tagName){
        return driver.findElements(By.tagName(tagName)).size();
    }
}
