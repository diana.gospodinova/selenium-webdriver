package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EntryAdPage {
    private WebDriver driver;
    private By modalTitle = By.xpath(".//div[@class='modal-title']//h3");
    private By modalFooter = By.xpath(".//div[@class='modal-footer']//p");

    public EntryAdPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getModalTitleText(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(modalTitle));
        return driver.findElement(modalTitle).getText();
    }

    public void closeModalWindow(){
        driver.findElement(modalFooter).click();
    }
}
