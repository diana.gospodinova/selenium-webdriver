package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ContextMenuPage {
    private WebDriver driver;
    private By contextMenu = By.id("hot-spot");

    public ContextMenuPage(WebDriver driver) {
        this.driver = driver;
    }

    public void rightClickInBox(){
        WebElement menu = driver.findElement(contextMenu);
        Actions actions = new Actions(driver);
        actions.contextClick(menu).perform();
    }

    public String getResult(){
        return driver.switchTo().alert().getText();
    }

    public void clickToAccept(){
        driver.switchTo().alert().accept();
    }
}
