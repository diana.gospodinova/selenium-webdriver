package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShiftingContentPage {
    private WebDriver driver;
    private By menuElementsLink = By.linkText("Example 1: Menu Element");

    public ShiftingContentPage(WebDriver driver) {
        this.driver = driver;
    }

    public MenuElementsPage clickMenuElements(){
        driver.findElement(menuElementsLink);
        return new MenuElementsPage(driver);
    }
}
