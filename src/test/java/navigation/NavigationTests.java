package navigation;

import base.BaseTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NavigationTests extends BaseTests {
    @Test
    public void testNavigation(){
        // Arrange // Act
        homePage.clickDynamicLoading().clickExample1();
        getWindowManager().goBack();
        getWindowManager().refreshPage();
        getWindowManager().goForward();
        getWindowManager().goTo("https://google.com");
        // Assert
        assertEquals(getWindowManager().getWindowTitle(), "Google", "This is not the requested page");
    }

    @Test
    public void testSwitchTab(){
        // Arrange  // Act
        homePage.clickMultipleWindows().clickHere();
        getWindowManager().switchToTab("New Window");
        // Assert
        assertEquals(getWindowManager().getWindowTitle(), "New Window", "This is not the requested page");
    }
}
