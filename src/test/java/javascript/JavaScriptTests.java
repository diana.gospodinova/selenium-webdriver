package javascript;

import base.BaseTests;
import org.testng.annotations.Test;

public class JavaScriptTests extends BaseTests {
    @Test
    public void testScrollToTable(){
        // Arrange //Act // Assert
        homePage.clickLargeAndDeepDom().scrollToTable();
    }

    @Test
    public void testScrollToFifthParagraph(){
        // Arrange // Act // Assert
        homePage.clickInfiniteScroll().scrollToParagraph(5);
    }
}
