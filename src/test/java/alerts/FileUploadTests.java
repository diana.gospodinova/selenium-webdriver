package alerts;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.FileUploadPage;

import static org.testng.Assert.assertEquals;

public class FileUploadTests extends BaseTests {
    @Test
    public void testFileUpload(){
        // Arrange
        FileUploadPage fileUploadPage = homePage.clickFileUpload();
        // Act
        fileUploadPage.uploadFile("C:/Users/Didi/SeleniumWebDriver/resources/file.txt");
        // Assert
        assertEquals(fileUploadPage.getUploadedFiles(), "file.txt", "Result text incorrect");
    }
}
