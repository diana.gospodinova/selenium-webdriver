package alerts;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.AlertsPage;

import static org.testng.Assert.assertEquals;

public class AlertTests extends BaseTests {
    @Test
    public void testAcceptAlert(){
        // Arrange
        AlertsPage alertsPage = homePage.clickJavaScriptAlerts();
        alertsPage.triggerAlert();
        // Act
        alertsPage.alert_clickToAccept();
        // Assert
        assertEquals(alertsPage.getResult(),"You successfuly clicked an alert", "Result text incorrect");
    }

    @Test
    public void testGetTextFromAlert(){
        // Arrange
        AlertsPage alertsPage = homePage.clickJavaScriptAlerts();
        alertsPage.triggerConfirm();
        // Act
        String text = alertsPage.alert_getText();
        alertsPage.alert_clickToDismiss();
        // Assert
        assertEquals(text, "I am a JS Confirm", "Result text incorrect");
    }

    @Test
    public void testSetInputInAlert(){
        // Arrange
        AlertsPage alertsPage = homePage.clickJavaScriptAlerts();
        alertsPage.triggerPrompt();
        String text = "TAU rocks!";
        // Act
        alertsPage.alert_setInput(text);
        alertsPage.alert_clickToAccept();
        // Assert
        assertEquals(alertsPage.getResult(), "You entered: " + text, "Result text incorrect");


    }
}
