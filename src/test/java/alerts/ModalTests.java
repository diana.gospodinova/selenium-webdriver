package alerts;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.EntryAdPage;

import static org.testng.Assert.assertEquals;

public class ModalTests extends BaseTests {
    @Test
    public void testGetTitleFromModal(){
        // Arrange
        EntryAdPage entryAdPage = homePage.clickEntryAd();
        // Act
        String result = entryAdPage.getModalTitleText();
        entryAdPage.closeModalWindow();
        // Assert
        assertEquals(result, "THIS IS A MODAL WINDOW", "Result text incorrect");
    }
}
