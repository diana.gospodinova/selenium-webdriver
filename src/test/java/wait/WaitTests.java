package wait;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.DynamicLoadingExample1Page;
import pages.DynamicLoadingExample2Page;

import static org.testng.Assert.assertEquals;

public class WaitTests extends BaseTests {
    @Test
    public void testWaitUntilHidden(){
        // Arrange
        DynamicLoadingExample1Page loadingPage = homePage.clickDynamicLoading().clickExample1();
        // Act
        loadingPage.clickStart();
        // Assert
        assertEquals(loadingPage.getLoadedText(), "Hello World!", "Result text incorrect");
    }

    @Test
    public void testWaitUntilVisible(){
        // Arrange
        DynamicLoadingExample2Page loadingPage = homePage.clickDynamicLoading().clickExample2();
        // Act
        loadingPage.clickStart();
        // Assert
        assertEquals(loadingPage.getLoadedText(), "Hello World!", "Result text incorrect");
    }
}
