package cookies;

import base.BaseTests;
import org.openqa.selenium.Cookie;
import org.testng.annotations.Test;
import utils.CookieManager;

import static org.testng.Assert.assertFalse;

public class CookiesTests extends BaseTests {
    @Test
    public void testDeleteCookie(){
        // Arrange
        CookieManager cookieManager = getCookieManager();
        String cookieName = "optimizelyBuckets";
        Cookie cookie = cookieManager.buildCookie("optimizelyBuckets", "%7B%TD");
        // Act
        cookieManager.deleteCookie(cookie);
        // Assert
        assertFalse(cookieManager.isCookiePresent(cookie), "Cookie was not deleted");
    }
}
