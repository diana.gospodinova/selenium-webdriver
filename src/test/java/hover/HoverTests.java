package hover;

import base.BaseTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class HoverTests extends BaseTests {
    @Test
    public void testHoverUser1(){
        // Arrange
        var hoversPage = homePage.clickHovers();
        // Act
        var caption = hoversPage.hoverOverFigure(1);
        // Assert
        assertTrue(caption.isCaptionDisplayed());
        assertEquals(caption.getTitle(), "name: user1");
        assertEquals(caption.getLinkText(), "View profile");
        assertTrue(caption.getLink().endsWith("/users/1"));
    }
}
