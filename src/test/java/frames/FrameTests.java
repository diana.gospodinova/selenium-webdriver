package frames;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.WysiwygEditorPage;

import static org.testng.Assert.assertEquals;

public class FrameTests extends BaseTests {
    @Test
    public void testWysiwyg(){
        // Assert
        WysiwygEditorPage editorPage = homePage.clickSysiwygEditor();
        editorPage.clearTextArea();

        String text1 = "Hello ";
        String text2 = "world";
        // Act
        editorPage.setTextArea(text1);
        editorPage.decreaseIndentation();
        editorPage.setTextArea(text2);
        // Assert
        assertEquals(editorPage.getTextFromEditor(), text1 + text2, "Result text incorrect");

    }
}
