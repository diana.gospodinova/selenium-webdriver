package home;

import base.BaseTests;
import org.openqa.selenium.Dimension;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class HomePageBaseTests  extends BaseTests {

    @Test
    public void getTitle() {
        // Arrange
        String title = "The Internet";
        // Act
        String result = homePage.getTitle();
        // Assert
        assertEquals(result, title);
    }

    @Test
    public void getWindowSize() {
        // Arrange
        Dimension size = new Dimension(600, 200);
        homePage.resizeWindow(600, 200);
        // Act
        Dimension result = homePage.getWindowSize();
        // Assert
        assertEquals(result, size);
    }

    @Ignore
    @Test
    public void maximizeWindow(){
        // Arrange
        Dimension size = new Dimension(1382, 744);
        // Act
        homePage.maximizeWindow();
        // Assert
        assertEquals(homePage.getWindowSize(), size);
    }

    @Test
    public void resizeWindow() {
        // Arrange
        Dimension size = new Dimension(600, 200);
        // Act
        homePage.resizeWindow(600, 200);
        // Assert
        assertEquals(homePage.getWindowSize(), size);
    }

    @Ignore
    @Test
    public void setFullScreenWindow() {
        // Arrange
        Dimension expectedResult = new Dimension(1366, 768);
        // Act
        homePage.setFullScreenWindowSize();
        // Assert
        assertEquals(homePage.getWindowSize(), expectedResult);
    }

    @Test
    public void findShiftingContentElement() {
        // Arrange
        String expectedResult = "Shifting Content";
        // Act
        String inputLink = homePage.findShiftingContent();
        // Assert
        assertEquals(inputLink, expectedResult);
    }
}
