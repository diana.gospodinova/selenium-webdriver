package additional_tasks;

import base.BaseTests;
import pages.*;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AdditionalTasksTests extends BaseTests {
    @Test
    public void countElements() {
        // Arrange
        int expectedResult = 5;
        ShiftingContentPage shiftingContentPage = homePage.clickShiftingContent();
        MenuElementsPage menuElementsPage = shiftingContentPage.clickMenuElements();
        // Act
        int result = menuElementsPage.countElementsByTagName("a");
        // Assert
        assertEquals(result, expectedResult);
    }

    @Test
    public void testForgotPassword(){
        // Arrange
        ForgotPasswordPage forgotPasswordPage = homePage.clickForgotPassword();
        forgotPasswordPage.setEmail("TAU@example.com");
        // Act
        EmailConfirmationPage emailConfirmationPage = forgotPasswordPage.clickRetrievePasswordButton();
        // Assert
        assertTrue(emailConfirmationPage.getConfirmationText().contains("Your e-mail's been sent!"),
                "Confirmation is unsuccessful");
    }

    @Test
    public void testHorizontalSlider(){
        // Arrange
        String value = "4";
        HorizontalSliderPage horizontalSliderPage = homePage.clickHorizontalSlider();
        // Act
        horizontalSliderPage.setSliderValue(value);
        // Assert
        assertEquals(horizontalSliderPage.getSliderValue(), value, "Slider value is incorrect");
    }

    @Test
    public void testContextMenu(){
        // Arrange
        ContextMenuPage contextMenuPage = homePage.clickContextMenu();
        // Act
        contextMenuPage.rightClickInBox();
        String result = contextMenuPage.getResult();
        contextMenuPage.clickToAccept();
        // Assert
        assertEquals(result, "You selected a context menu", "Result text incorrect");
    }

    @Test
    public void testFrameText() {
        // Arrange //Act
        NestedFramesPage nestedFramesPage = homePage.clickFrames().clickNestedFrames();
        // Assert
        assertEquals(nestedFramesPage.getLeftFrameText(), "LEFT", "Result text incorrect");
        assertEquals(nestedFramesPage.getBottomFrameText(), "BOTTOM", "Result text incorrect");
    }

    @Test
    public void testWindowsNavigation(){
        // Arrange
        DynamicLoadingExample2Page page = homePage.clickDynamicLoading().rightClickOnExample2Link();
        // Act
        getWindowManager().switchToNewTab();
        // Assert
        assertTrue(page.isStartButtonDisplayed(), "Start button is not displayed");
    }
}
