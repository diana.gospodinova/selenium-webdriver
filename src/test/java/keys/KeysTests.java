package keys;

import base.BaseTests;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class KeysTests extends BaseTests {
    @Test
    public void testBackspace(){
        // Arrange
        var keyPage = homePage.clickKeyPresses();
        // Act
        keyPage.enterText("A" + Keys.BACK_SPACE);
        // Assert
        assertEquals(keyPage.getResult(), "You entered: BACK_SPACE");
    }

    @Test
    public void testPi(){
        // Arrange
        var keyPage = homePage.clickKeyPresses();
        // Act
        keyPage.enterPi();
        // Assert
    }
}
