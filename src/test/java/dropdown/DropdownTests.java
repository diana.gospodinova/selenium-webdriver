package dropdown;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.DropdownPage;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class DropdownTests extends BaseTests {
    @Test
    public void testSelectOption(){
        // Arrange
        var dropDownPage = homePage.clickDropDown();
        String option = "Option 1";
        // Act
        dropDownPage.selectFromDropDown(option);
        var selectedOptions = dropDownPage.getSelectedOption();
        // Assert
        assertEquals(selectedOptions.size(), 1, "Incorrect number of selections");
        assertTrue(selectedOptions.contains(option), "Option not selected");
    }

    @Test
    public void testMultipleSelectOptions(){
        // Arrange
        DropdownPage dropDownPage = homePage.clickDropDown();
        dropDownPage.addMultipleAttribute();
        var optionsToSelect = List.of("Option 1", "Option 2");
        // Act
        //optionsToSelect.forEach(dropDownPage::selectFromDropDown);
        dropDownPage.selectFromDropDown(optionsToSelect.get(0));
        dropDownPage.selectFromDropDown(optionsToSelect.get(1));
        var selectedOptions = dropDownPage.getSelectedOption();
        // Assert
        assertTrue(selectedOptions.containsAll(optionsToSelect), "All options were not selected");
        assertEquals(selectedOptions.size(), optionsToSelect.size(), "Incorrect number of selected options");
    }
}
